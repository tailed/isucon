package main

import (
	"../template"
	"os"
	"path"
)

func getTemplPath(filename string) string {
	return path.Join("templates", filename)
}



func main() {
	w := os.Stdout
	template.Must(template.ParseFiles(
		getTemplPath("layout.html"),
		getTemplPath("login.html")),
	).PreCompile(w)

	template.Must(template.ParseFiles(
		getTemplPath("layout.html"),
		getTemplPath("register.html")),
	).PreCompile(w)

	t, err := template.New("layout.html").ParseFiles(
		getTemplPath("layout.html"),
		getTemplPath("index.html"),
		getTemplPath("posts.html"),
		getTemplPath("post.html"),
	)
	if err != nil {
		panic(err)
	}
	err = t.PreCompile(w)
	if err != nil { panic(err) }


	template.Must(template.New("layout.html").ParseFiles(
		getTemplPath("layout.html"),
		getTemplPath("user.html"),
		getTemplPath("posts.html"),
		getTemplPath("post.html"),
	)).PreCompile(w)

}
