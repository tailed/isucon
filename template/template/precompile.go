// Copyright 2011 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package template

import (
	"fmt"
	"io"
	"reflect"
	"./parse"
	"os"
	"path"
	"strings"
	"runtime"
	"regexp"
	"io/ioutil"
	"sync"
)

var rewrite_mutex sync.Mutex

// Rewrite commands 
func (t *Template) rewrite_code(n_stack int, data interface{}) {
	rewrite_mutex.Lock()
	defer rewrite_mutex.Unlock()

	pc, filename, line, ok := runtime.Caller(n_stack + 1)
	if !ok { panic("Could not detect caller.") }
	caller := runtime.FuncForPC(pc).Name()
	pkg := caller
	if pos := strings.LastIndex(caller, "."); pos != -1 {
		pkg = pkg[0:pos]
	} else {
		panic("Could not find package: " + pkg)
	}

	file_basename := regexp.MustCompile("^[a-zA-Z]*").FindString(path.Base(filename))
	generated_filename := fmt.Sprintf("%s/template_%s_%d.go", path.Dir(filename), file_basename, line)
	if _, err := os.Stat(generated_filename); err == nil {
		/* file already exists */
		fmt.Fprintf(os.Stderr, "Skiped generating " + generated_filename + " because it exists\n")
		return
	}

	fmt.Fprintf(os.Stderr, "Writing the precompiled template code: %s\n", generated_filename)

	file, err := os.Create(generated_filename)
	if err != nil { panic(err) }
	typ := reflect.TypeOf(data)
	data_type := typ.String()
	data_type = strings.Replace(data_type, pkg + ".", "", -1) // remove all references to pkg
	funcname := fmt.Sprintf("PrecompiledTmpl_%s_%d", file_basename, line)
	t.precompile(file, funcname, data_type)
	if err != nil { panic(err) }

	// rewrite code
	rewrite_at(filename, line, funcname)
}

func rewrite_at(filename string, line_num int, funcname string) {
	fmt.Fprintf(os.Stderr, "Rewriting %s of line %d\n", filename, line_num)
	code, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}
	code_lines := strings.Split(string(code), "\n")
	code_lines[line_num-1] = regexp.MustCompile(`Execute(Template)?\((.*)`).ReplaceAllString(code_lines[line_num-1], funcname + "($2")
	err = ioutil.WriteFile(filename, []byte(strings.Join(code_lines, "\n")), os.ModePerm)
	if err != nil {
		panic(err)
	}
}

var zero_pc = variable{name: "zero"}

func chomp_leading_dollar_pc(name string) string {
	if name[0] == '$' {
		return name[1:]
	} else {
		return name
	}
}

func (s *state) push_pc(name string, value variable) {
	fmt.Fprintln(s.wr, chomp_leading_dollar_pc(name) + " := " + value.name)
	s.vars = append(s.vars, variable{name, value.value})
}

// printValue writes the textual representation of the value to the output of
// the template.
func (s *state) printValue_pc(n parse.Node, v variable) {
	s.at(n)
	_, err := fmt.Fprintf(s.wr, "fmt.Fprint(w, %s)\n", v.name)
	if err != nil {
		s.writeError(err)
	}
}


func (t *Template) PreCompile(wr io.Writer) (err error) {
	return t.precompile(wr, t.name, "interface{}")
}

func (t *Template) precompile(wr io.Writer, funcname string, data_type string) (err error){
	defer errRecover(&err)
	value := reflect.ValueOf(struct{}{}) /* dummy */
	state := &state{
		tmpl: t,
		wr:   wr,
		vars: []variable{{"$", value}},
	}
	if t.Tree == nil || t.Root == nil {
		state.errorf("%q is an incomplete or empty template", t.Name())
	}
	fmt.Fprintln(state.wr, "package main")
	fmt.Fprintln(state.wr, "import (")
	fmt.Fprintln(state.wr, "	\"io\"")
	fmt.Fprintln(state.wr, "	\"fmt\"")
	fmt.Fprintln(state.wr, "	\"text/template\"")
	fmt.Fprintln(state.wr, ")")
	fmt.Fprintf(state.wr, "func %s(w io.Writer, data %s) error {\n", funcname, data_type)
	state.walk_pc(variable{"data", value}, t.Root)
	fmt.Fprintf(state.wr, "return nil")
	fmt.Fprintf(state.wr, "}\n")
	return
}

func (s *state) walk_pc(dot variable, node parse.Node) {
	s.at(node)
	switch node := node.(type) {
	case *parse.ActionNode:
		// Do not pop variables so they persist until next end.
		// Also, if the action declares variables, don't print the result.
		val := s.evalPipeline_pc(dot, node.Pipe, false)
		if len(node.Pipe.Decl) == 0 {
			s.printValue_pc(node, val)
		}
	case *parse.IfNode:
		s.walkIfOrWith_pc(parse.NodeIf, dot, node.Pipe, node.List, node.ElseList)
	case *parse.ListNode:
		for _, node := range node.Nodes {
			s.walk_pc(dot, node)
		}
	case *parse.RangeNode:
		s.walkRange_pc(dot, node)
	case *parse.TemplateNode:
		s.walkTemplate_pc(dot, node)
	case *parse.TextNode:
		if _, err := fmt.Fprintf(s.wr, "fmt.Fprint(w, `%s`)\n", node.Text); err != nil {
			s.writeError(err)
		}
	case *parse.WithNode:
		s.walkIfOrWith_pc(parse.NodeWith, dot, node.Pipe, node.List, node.ElseList)
	default:
		s.errorf("unknown node: %s", node)
	}
}

func (s *state) walkTemplate_pc(dot variable, t *parse.TemplateNode) {
	s.at(t)
	tmpl := s.tmpl.tmpl[t.Name]
	if tmpl == nil {
		s.errorf("template %q not defined", t.Name)
	}
	if s.depth == maxExecDepth {
		s.errorf("exceeded maximum template depth (%v)", maxExecDepth)
	}
	// Variables declared by the pipeline persist.
	dot = s.evalPipeline_pc(dot, t.Pipe, false)
	newState := *s
	newState.depth++
	newState.tmpl = tmpl
	// No dynamic scoping: template invocations inherit no variables.
	newState.vars = []variable{{"$", dot.value}}
	newState.walk_pc(dot, tmpl.Root)
}

func (s *state) walkRange_pc(dot variable, r *parse.RangeNode) {
	s.at(r)
	defer s.pop(s.mark())
	val, _ := indirect_pc(s.evalPipeline_pc(dot, r.Pipe, true))
	// mark top of stack before any variables in the body are pushed.
	mark := s.mark()
	elem_name := "iter"
	if len(r.Pipe.Decl) > 0 {
		elem_name = s.vars[len(s.vars)-1].name
	}
	elem := variable{elem_name, zero_pc.value}
	index_name := "_"
	if len(r.Pipe.Decl) > 1 {
		index_name = s.vars[len(s.vars)-2].name
	}
	fmt.Fprintf(s.wr, "for %s, %s := range %s {\n", chomp_leading_dollar_pc(index_name), chomp_leading_dollar_pc(elem_name), val.name)
	s.walk_pc(elem, r.List)
	s.pop(mark)
	fmt.Fprintf(s.wr, "}\n")
	if r.ElseList != nil {
		s.errorf("range's else is not implemented")
		//		s.walk_pc(dot, r.ElseList)
	}
}

// walkIfOrWith walks an 'if' or 'with' node. The two control structures
// are identical in behavior except that 'with' sets dot.
func (s *state) walkIfOrWith_pc(typ parse.NodeType, dot variable, pipe *parse.PipeNode, list, elseList *parse.ListNode) {
	defer s.pop(s.mark())
	val := s.evalPipeline_pc(dot, pipe, false)
	_, ok := isTrue(val.value) /* ignore truth value! i.e. output both of branches */
	if !ok {
		s.errorf("if/with can't use %v", val)
	}
	fmt.Fprintf(s.wr, "if truth, _ := template.IsTrue(%s); truth {\n", val.name)
	if typ == parse.NodeWith {
		s.walk_pc(val, list) /* Maybe fail because val might be nil */
	} else {
		s.walk_pc(dot, list)
	}
	fmt.Fprint(s.wr, "}")
	if elseList != nil {
		fmt.Fprint(s.wr, " else {\n")
		s.walk_pc(dot, elseList)
		fmt.Fprint(s.wr, "}")
	}
	fmt.Fprint(s.wr, "\n")
}

func (s *state) evalPipeline_pc(dot variable, pipe *parse.PipeNode, ignoreDecl bool) (value variable) {
	if pipe == nil {
		return
	}
	s.at(pipe)
	for _, cmd := range pipe.Cmds {
		value = s.evalCommand_pc(dot, cmd, value) // previous value is this one's final arg.
		// If the object has type interface{}, dig down one level to the thing inside.
		if value.value.Kind() == reflect.Interface && value.value.Type().NumMethod() == 0 { /* XXX ??? */
			value.value = reflect.ValueOf(value.value.Interface()) // lovely!
		}
	}
	for _, variable := range pipe.Decl {
		if ignoreDecl { /* range case */
			s.push(variable.Ident[0], value.value)
		} else {
			s.push_pc(variable.Ident[0], value)
		}
	}
	return value
}

func (s *state) evalCommand_pc(dot variable, cmd *parse.CommandNode, final variable) variable {
	firstWord := cmd.Args[0]
	switch n := firstWord.(type) {
	case *parse.FieldNode:
		return s.evalFieldNode_pc(dot, n, cmd.Args, final)
	case *parse.ChainNode:
		panic("not implemented chain")
		//		return s.evalChainNode(dot.value, n, cmd.Args, final)
	case *parse.IdentifierNode:
		// Must be a function.
		return s.evalFunction_pc(dot, n, cmd, cmd.Args, final)
	case *parse.PipeNode:
		// Parenthesized pipeline. The arguments are all inside the pipeline; final is ignored.
		panic("not implemented pipe")
		//		return s.evalPipeline(dot.value, n)
	case *parse.VariableNode:
		return s.evalVariableNode_pc(dot, n, cmd.Args, final)
	}
	s.at(firstWord)
	switch word := firstWord.(type) {
	case *parse.BoolNode:
		return literalWrapper_pc(reflect.ValueOf(word.True))
	case *parse.DotNode:
		return dot
	case *parse.NilNode:
		s.errorf("nil is not a command")
	case *parse.NumberNode:
		return literalWrapper_pc(s.idealConstant(word))
	case *parse.StringNode:
		return literalWrapper_pc_string(reflect.ValueOf(word.Text))
	}
	s.errorf("can't evaluate command %q", firstWord)
	panic("not reached")
}

func (s *state) evalFieldNode_pc(dot variable, field *parse.FieldNode, args []parse.Node, final variable) variable {
	s.at(field)
	return s.evalFieldChain_pc(dot, dot, field, field.Ident, args, final)
}

/*
func (s *state) evalChainNode(dot reflect.Value, chain *parse.ChainNode, args []parse.Node, final reflect.Value) reflect.Value {
	s.at(chain)
	if len(chain.Field) == 0 {
		s.errorf("internal error: no fields in evalChainNode")
	}
	if chain.Node.Type() == parse.NodeNil {
		s.errorf("indirection through explicit nil in %s", chain)
	}
	// (pipe).Field1.Field2 has pipe as .Node, fields as .Field. Eval the pipeline, then the fields.
	pipe := s.evalArg(dot, nil, chain.Node)
	return s.evalFieldChain(dot, pipe, chain, chain.Field, args, final)
}

*/

func (s *state) evalVariableNode_pc(dot variable, vari *parse.VariableNode, args []parse.Node, final variable) variable {
	// $x.Field has $x as the first ident, Field as the second. Eval the var, then the fields.
	s.at(vari)
	value := s.varValue(vari.Ident[0])
	pair := variable{chomp_leading_dollar_pc(vari.Ident[0]), value}
	if len(vari.Ident) == 1 {
		return pair
	}
	return s.evalFieldChain_pc(dot, pair, vari, vari.Ident[1:], args, final)
}

// evalFieldChain evaluates .X.Y.Z possibly followed by arguments.
// dot is the environment in which to evaluate arguments, while
// receiver is the value being walked along the chain.
func (s *state) evalFieldChain_pc(dot, receiver variable, node parse.Node, ident []string, args []parse.Node, final variable) variable {
	n := len(ident)
	for i := 0; i < n-1; i++ {
		receiver = s.evalField_pc(dot, ident[i], node, nil, zero_pc, receiver)
	}
	// Now if it's a method, it gets the arguments.
	return s.evalField_pc(dot, ident[n-1], node, args, final, receiver)
}

func (s *state) evalFunction_pc(dot variable, node *parse.IdentifierNode, cmd parse.Node, args []parse.Node, final variable) variable {
	s.at(node)
	name := node.Ident
	return s.evalCall_pc(dot, cmd, name, args, final)
}

// indirect returns the item at the end of indirection, and a bool to indicate if it's nil.
func indirect_pc(v variable) (rv variable, isNil bool) {
	for ; v.value.Kind() == reflect.Ptr || v.value.Kind() == reflect.Interface; v = (variable{"*(" + v.name + ")", v.value.Elem()}) {
		if v.value.IsNil() {
			return v, true
		}
	}
	return v, false
}

// evalField evaluates an expression like (.Field) or (.Field arg1 arg2).
// The 'final' argument represents the return value from the preceding
// value of the pipeline, if any.
func (s *state) evalField_pc(dot variable, fieldName string, node parse.Node, args []parse.Node, final, receiver variable) variable {
	return variable{receiver.name + "." + fieldName, zero_pc.value}
}

// evalCall executes a function or method call. If it's a method, fun already has the receiver bound, so
// it looks just like a function call. The arg list, if non-nil, includes (in the manner of the shell), arg[0]
// as the function itself.
func (s *state) evalCall_pc(dot variable, node parse.Node, name string, args []parse.Node, final variable) variable {
	if args != nil {
		args = args[1:] // Zeroth arg is function name/node; not passed to function.
	}
	numIn := len(args)
	if final.value.IsValid() {
		numIn++
	}
	// Build the arg list.
	argv := make([]variable, numIn)
	for i := 0; i < len(args); i++ {
		argv[i] = s.evalArg_pc(dot, args[i])
	}
	argv_names := ""
	for i, v := range argv {
		if i > 0 {
			argv_names += ", "
		}
		argv_names += v.name
	}
	return variable{name + "(" + argv_names + ")", zero_pc.value}
}

// validateType guarantees that the value is valid and assignable to the type.
func (s *state) validateType_pc(value variable, typ reflect.Type) variable {
	return variable{value.name, s.validateType(value.value, typ)}
}

func (s *state) evalArg_pc(dot variable, n parse.Node) variable {
	s.at(n)
	switch arg := n.(type) {
	case *parse.DotNode:
		return dot
	case *parse.NilNode:
		return zero_pc /* ??? */
//		s.errorf("cannot assign nil to %s", typ)
	case *parse.FieldNode:
		return s.evalFieldNode_pc(dot, arg, []parse.Node{n}, zero_pc)
	case *parse.VariableNode:
		return s.evalVariableNode_pc(dot, arg, nil, zero_pc)
	case *parse.PipeNode:
		panic("not implemented")
		//		return s.validateType(s.evalPipeline(dot, arg), typ)
	case *parse.IdentifierNode:
		panic("not implemented")
		//		return s.validateType(s.evalFunction(dot, arg, arg, nil, zero), typ)
	case *parse.ChainNode:
		panic("not implemented")
		//		return s.validateType(s.evalChainNode(dot, arg, nil, zero), typ)
	}

	res := s.evalEmptyInterface_pc(dot, n)
	return variable{res.name, reflect.ValueOf(res.value)}
}

func literalWrapper_pc(v reflect.Value) variable {
	return variable{fmt.Sprint(v), v}
}
func literalWrapper_pc_string(v reflect.Value) variable {
	return variable{fmt.Sprintf("`%s`", v), v}
}

func (s *state) evalEmptyInterface_pc(dot variable, n parse.Node) variable {
	s.at(n)
	switch n := n.(type) {
	case *parse.BoolNode:
		return literalWrapper_pc(reflect.ValueOf(n.True))
	case *parse.DotNode:
		return dot
	case *parse.FieldNode:
		return s.evalFieldNode_pc(dot, n, nil, zero_pc)
	case *parse.IdentifierNode:
		return s.evalFunction_pc(dot, n, n, nil, zero_pc)
	case *parse.NilNode:
		// NilNode is handled in evalArg, the only place that calls here.
		s.errorf("evalEmptyInterface: nil (can't happen)")
	case *parse.NumberNode:
		return literalWrapper_pc(s.idealConstant(n))
	case *parse.StringNode:
		return literalWrapper_pc_string(reflect.ValueOf(n.Text))
	case *parse.VariableNode:
		return s.evalVariableNode_pc(dot, n, nil, zero_pc)
	case *parse.PipeNode:
		return s.evalPipeline_pc(dot, n, false)
	}
	s.errorf("can't handle assignment of %s to empty interface argument", n)
	panic("not reached")
}
