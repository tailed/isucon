package main

import (
	"./template"
	"fmt"
	"os"
)

type Job struct {
	Name string
}
type Person struct {
	UserName string
	MyJobs []Job
	Age int
}

func mul(a, b int) int { return a * b }
func gt(a, b int) bool { return a > b }


func main() {
	funcMap := template.FuncMap{
		"add": func(a, b int) int { return a + b },
		"sub": func(a, b int) int { return a - b },
		"mul": func(a, b int) int { return a * b },
		"div": func(a, b int) int { return a / b },
		"sq": func(a int) int { return a * a },
		"check": func(a string) string { return a },
	}

	{}

	t, err := template.New("example").Funcs(funcMap).Parse(`
	hello {{.UserName}} {{mul .Age 2}} {{if gt 2 3}}ok{{else}}ng{{end}}!
	{{ $myname := .UserName }}
	have job?
	{{range $i, $p := .MyJobs}}
	{{$myname}} {{$i}}th job is {{$p.Name}}
	{{end}}
	`)
	if err != nil { panic(err) }
	p := (/*struct {
			UserName string
				MyJobs []Job
					Age int
				} */ Person{UserName: "Astaxie", Age: 20, MyJobs: []Job{Job{"student"}, Job{"dc"}}})
				fmt.Printf("Type: %T\n", p)
	//	buffer := new(bytes.Buffer)

	err = t.Execute(os.Stdout, p)
}
