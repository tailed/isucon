require_relative './utils.rb'
require_relative './timeline.rb'
require 'pp'

class GoreplayAnalyzer
	Delim = "\x0a\xf0\x9f\x90\xb5\xf0\x9f\x99\x88\xf0\x9f\x99\x89\x0a"

	def analyze(response)
		pos = response.index("\n")
		head, response = response[0..pos-1], response[pos+1..-1]
		# head, response = response.split("\n", 2) # ==> invalid byte sequence
		flag, id, time, latency = head.split(" ")
		#p [flag, id, time, latency]
		flag = flag.to_i
		time = time.to_i # * 1e-9
		@base_time = @base_time || time
		time -= @base_time
		time = (time * 1e-6).round
		latency = (latency.to_f * 1e-6).round
		case flag
		when 1
			@id2data[id][:request] = parse_http_request(response)
			# skip
		when 2
			@id2data[id][:response] = parse_http_response(response)
			@id2data[id][:latency] = latency
			@id2data[id][:time] = time
		else
			raise "Unexpected flag: #{flag}"
		end

		h = @id2data[id]
		if h[:request] && h[:response]
			raise "Could not find id:#{id}" if !@id2data[id]
			head = h[:request][:head]
			cookies = h[:request][:cookies]
			h_response = h[:response]
			(h_response[:headers][:set_cookie] || "").to_s =~ /UniqueID=(\d+)/
			unique_id = ($1 || -1).to_i
			unique_id = $1.to_i if cookies =~ /UniqueID=(\d+)/
			# $stderr.puts "#{unique_id} #{head}"
			time = h[:time]
			latency = h[:latency]
			@threadid2queue[unique_id] << { :head => head, :time => time }
#			@timeline << { "id" => (@curid += 1), "group" => unique_id, "content" => "#{unique_id} #{head}", "start" => time, "end" => time + latency }
		end

	end

	def parse(content)
		@curid = 1
		@timeline = Timeline.new
		@base_time = nil
		@id2data = Hash.new {|h, k| h[k] = {}}
		@threadid2queue = Hash.new {|h, k| h[k] = []}

		cnt = 0
		cur_pos = 0
		content.each_line(Delim) do |res|
			analyze(res)
			cnt += 1
#			break	if cnt > 100
		end

		analyze_scenario()

		res = @timeline.htmlify(group_numbering: true)
		outfile = "./visualize/visualize.html"
		$stderr.puts "Writing to #{outfile}"
		File.open(outfile, "w") do|f|
			f.write res
		end
		@data
	end

	def histogram_of(arr)
		arr.inject(Hash.new(0)) {|h, e| h.tap {|h| h[e] += 1} }
	end
	def analyze_scenario()
		merged_ids = Hash.new {|h, k| h[k] = []}
		#scenarios = Hash.new(0)
		id2interval = {}
		id2h = {}
		@threadid2queue.each do |id, requests|
			times = requests.map {|r| r[:time]}
			times.concat(id2interval[id]) if id2interval[id]
			id2interval[id] = [times.min, times.max] # update interval

			reqs = requests.map {|r|
				req, url = parse_request_1stline(r[:head])
				"#{req} #{url}"
			}
			h = histogram_of(reqs)
			h = h.map {|a, b| [a, b]}.sort
			id2h[id] = h
			merged_ids[h] << id
		end
		idss = merged_ids.to_a.sort_by {|h, ids| -ids.length}
=begin
		idss.each do |h, ids|
			ids.each do |id|
				@timeline.add(h.to_s, h.to_s, id2interval[id][0], id2interval[id][1])
			end
		end
=end
		idss = idss.map {|h, ids| ids}

		3.times do
		idss.length.times do |i|
			# idss[i]とマージできるものを探す
			for j in i+1..idss.length-1
				overlap_length = idss[i].inject(0) {|sum, id|
					sum + idss[j].inject(0) {|sum2, jd|
						a, b = id2interval[id]
						c, d = id2interval[jd]
						len = [0, [b, d].min - [a, c].max].max
						sum2 + len}
				}
				p [i, j, overlap_length]
				if overlap_length == 0 then
					$stderr.puts "merge #{i} #{j}"
					idss[i].concat(idss[j])
					idss.delete_at(j)
					break
				end
			end
		end
		end

		idss.each_with_index do |ids, group_id|
			ids.each do |id|
				h = id2h[id]
				@timeline.add(group_id, h.to_s, id2interval[id][0], id2interval[id][1])
			end
		end

=begin
		idss.each do |ids|
			hs = ids.map do |id|
				requests = @threadid2queue[id]
				reqs = requests.map {|r|
					req, url = parse_request_1stline(r[:head])
					"#{req} #{url}"
				}
				h = histogram_of(reqs)
				h
			end
			hs.uniq!
			puts ids.length
			pp hs
		end
=end
	end
end

if __FILE__ == $0 then
	require 'zlib'
	ga = GoreplayAnalyzer.new
	content = Zlib::GzipReader.open('requests_0.gz') {|gz| gz.read}
	ga.parse(content)
end

