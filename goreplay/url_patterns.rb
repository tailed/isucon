URL_PATTERNS = ['\A(.*\?).*\Z', '\A/keyword/', '\A/css', '\A/js', '\A/img']

# 正規表現にマッチするURLパターンを一つにまとめる
def apply_url_patterns(url)
	URL_PATTERNS.each do |s|
		if url =~ /#{s}/ then
			return $1 || $& # カッコで囲まれた部分 or マッチした部分全体
		end
	end
	return url
end
