require_relative './utils.rb'

class PerformanceStats
	def initialize()
		@url2count = Hash.new(0)
		@url2time = Hash.new(0.0)
		@url2data = Hash.new {|h, k| h[k] = Hash.new(0) }
	end

	def string_of(x)
		if x.class == Float then
			"%.4f" % x
		else
			x.to_s
		end
	end

	def get_url_of(req_1stline) # the first line of request header
		req, url = parse_request_1stline(req_1stline)
		"#{req.ljust(4)} #{url}"
	end

	def add(req_1stline, time, others = {})
		url = get_url_of(req_1stline)
		@url2count[url] += 1
		@url2time[url] += time.to_f
		others.each do |h, k|
			@url2data[h][url] += k
		end
	end

	def inspect()
		additional_stats = @url2data.keys
		table = []
		@url2count.keys.each do |url|
			line = [@url2time[url], @url2count[url], @url2time[url]/@url2count[url], url]
			additional_stats.each do |k|
				line.push(@url2data[k][url] / @url2count[url])
			end
			table.push line
		end
		table.sort! {|a, b| b <=> a}

		header = ["time", "count", "time/count", "URL"] + additional_stats
		table = [header] + table
		lengths = table.transpose.map {|col| col.map{|x| string_of(x).length }.max }

		table.map {|line|
			[lengths, line].transpose.map{|len, x| string_of(x).ljust(len)}.join("  ")
		}.join("\n")
	end
end


