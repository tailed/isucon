package main

import (
	"strconv"
	"fmt"
	"net/http"
)


var global_counter chan int
func init() {
	global_counter = make(chan int)
	go func() {
		i := 1
		for {
			global_counter <- i
			i += 1
		}
	}()
}

func appendUniqueID(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie("UniqueID")
	if err == http.ErrNoCookie {
		fmt.Println("Cookie Not found")
		http.SetCookie(w, &http.Cookie{
			Name: "UniqueID",
			Value: strconv.Itoa(<-global_counter),
			Path: "/",
		})
		return
	} else if err != nil {
		panic(err)
	} else {
		fmt.Println("UniqueID: ", cookie)
	}
}

/* // example
func myHandler(fn func(http.ResponseWriter, *http.Request)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		appendUniqueID(w, r)
		defer func() {
			if err := recover(); err != nil {
				fmt.Fprintf(os.Stderr, "%+v", err)
				debug.PrintStack()
				http.Error(w, http.StatusText(500), 500)
			}
		}()
		prepareHandler(fn)(w, r)
	}
}
*/

