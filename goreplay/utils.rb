require 'http'
require 'zlib'
require_relative './url_patterns.rb'

def parse_request_1stline(head)
	url = head.split(" ")[1]
	url = apply_url_patterns(url)
	req = head.split(" ")[0]
	[req, url]
end

def parse_http_response(response)
	parser = HTTP::Response::Parser.new
	parser << response
	headers = HTTP::Headers.coerce(parser.headers)

	if %w(deflate gzip x-gzip).include?(headers[:content_encoding])
		body = Zlib::Inflate.new(32 + Zlib::MAX_WBITS).inflate(parser.chunk)
	else
		body = parser.chunk
	end
	body ||= ""

	return { :headers => headers, :body => body, :status_code => parser.status_code }
end

def parse_http_request(request)
	pos = request.index("\r\n\r\n")
	head = request[0..pos-1]
	cookies = ""
	headers = head.split("\r\n")
	headers.each do |line|
		if line =~ /\ACookie\s*:\s*(.*)\Z/i
			cookies += $1 + "; "
		end
	end
	return { :head => headers[0], :cookies => cookies }
end

def hamming_distance(a, b)
	cnt = 0
	len = [a.length, b.length].max
	len.times do |i|
		cnt += 1 if a[i] != b[i]
	end
	return len > 0 ? cnt * 1.0 / len : 0
end

def distance_of_strings(a, b)
	cnt = 0
	c = a.split("\n")
	d = b.split("\n")
	len = [c.length, d.length].max
	len.times do |i|
		if !c[i] || !d[i]
			cnt += 1
		else
			cnt += hamming_distance(c[i], d[i])
		end
	end
	return len > 0 ? cnt * 1.0 / len : 0
end


