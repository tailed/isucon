# coding: utf-8
=begin
nginxのログファイルの解析

nginxの設定:
log_format  mainlog  '$status|$request_time|$msec|$request_length|$remote_addr|$remote_user|$time_local|$body_bytes_sent|$request|$http_referer|$http_user_agent|$http_x_forwarded_for|$connection';
access_log  /home/isucon/access.log  mainlog;

上の設定で/home/isucon/access.logにログファイルが出来るので、それを標準入力に流し込む

ログの内容: http://nginx.org/en/docs/http/ngx_http_log_module.html

Usage: ruby stat.rb [まとめたいURLのprefix] [prefix2...]  < access.log
例えば
ruby stat.rb  /memo /recent <access.log
とやると/memoとか/recentから始まるURLはまとまる。


$ ruby tools/stat.rb /diary/entry/ /diary/entries/ /diary/comment/ /profile/ /friends/
=end

require_relative './performance_stats.rb'


if __FILE__ == $0
	stats = PerformanceStats.new

	log_data = []
	while $stdin.gets
		keys = [:status, :request_time, :msec, :request_length, :remote_addr, :remote_user, :time_local, :body_bytes_sent, :request, :http_referer, :http_user_agent, :http_x_forwarded_for, :connection]
		values = $_.split("|")

		next if keys.length != values.length
		h = Hash[[keys, values].transpose]
		stats.add("#{h[:request][1]} #{h[:request]}", h[:elapsed_time])
	end

	puts stats.inspect
end



