#!/usr/bin/env ruby
require_relative './timeline.rb'
require_relative './performance_stats.rb'
require_relative './utils.rb'

require 'rainbow/ext/string' # for coloring
Rainbow.enabled = true # middlewareではttyだと判断されないらしい

require 'fileutils'
DIFF_FILE_DIR = "./diff/"
FileUtils.mkdir_p(DIFF_FILE_DIR)

class MyTimeoutException < StandardError; end


class Middleware
	def run(timeout: 10, verbose: false)
		stats = PerformanceStats.new()
		id2info = Hash.new {|h, k| h[k] = {}}
		total_distance = 0
		total_latency = 0.0
		n_processed = 0
		start_time = Time.now
		timeline = Timeline.new
		while $stdin.gets
			puts $_
			decoded = $_.scan(/../).map {|x| x.to_i(16).chr}.join
			head, response = decoded.split("\n", 2)
			flag, id, time, latency = head.split(" ")
			time = time.to_f * 1e-9 # seconds
			response ||= ""
			#$stderr.puts "#{flag} #{id}"

			flag = flag.to_i
			id2info[id][:flag] ||= 0
			id2info[id][:flag] += flag
			case flag
			when 1
				h = parse_http_request(response)
				request_summary = response.split("\n", 2)[0].chomp
				id2info[id][:req_1stline] = request_summary
				method, request_url, http_version = request_summary.split(" ")
				id2info[id][:request_summary] = "#{method} #{request_url}"
				id2info[id][:request_time] = time
				id2info[id][:request_cookies] = h[:cookies]
			when 2
				original_response = response
				h = parse_http_response(original_response)
				id2info[id][:original] = h
			when 3
				id2info[id][:elapsed_time] = latency.to_f * 1e-9
				replayed_response = response
				id2info[id][:replayed] = parse_http_response(replayed_response)
			else
				raise "Unknown type: #{flag.to_i}"
			end

			# 1,2,3が全て現れていたら最終的な処理をする
			h = id2info[id]
			if h[:flag] == 6
				score = distance_of_strings(h[:original][:body], h[:replayed][:body])
				if verbose && score > 0 then
					File.open("#{DIFF_FILE_DIR}/#{id}_old", "w") {|f| f.write h[:original][:body]}
					File.open("#{DIFF_FILE_DIR}/#{id}_new", "w") {|f| f.write h[:replayed][:body]}
				end
				if verbose then
					l = ""
					l << "#{h[:replayed][:status_code]}"
					if h[:replayed][:status_code] != h[:original][:status_code]
						l << " != #{h[:original][:status_code]}".color(:red) # expected response
					else
						l << "       "
					end
					l << " %#6.4f sec," % h[:elapsed_time]
					dist = "%#6.4f" % score
					dist = dist.color(:red) if score > 0
					l << " #{dist} distance,"
					l << " #{h[:request_summary].to_s.ljust(20)[0..19]},"
					l << " id #{id}"
					$stderr.puts l 
				end
				total_distance += score
				total_latency += h[:elapsed_time]
				n_processed += 1
				stats.add(h[:req_1stline], h[:elapsed_time], { "distance" => score })

				# get thread id
				(h[:original][:headers][:set_cookie] || "").to_s =~ /UniqueID=(\d+)/
				unique_id = ($1 || -1).to_i # get it from server's set-cookie
				cookies = h[:request_cookies]
				unique_id = $1.to_i if cookies =~ /UniqueID=(\d+)/ # get it from client's cookie
				# for visualizer
				start = h[:request_time]
				stop = h[:request_time] + h[:elapsed_time]
				start = (start * 1e3).round # ms
				stop = (stop * 1e3).round # ms
				timeline.add(unique_id, "#{unique_id} #{h[:request_summary]}", start, stop)
			end

			elapsed_time = Time.now - start_time
			if elapsed_time > timeout then
				raise MyTimeoutException, "The predetermined time (#{timeout} sec) has passed."
			end
		end
	rescue MyTimeoutException => e
		$stderr.puts e
		exit 1
	rescue Interrupt
		$stderr.puts "Interrupt"
		exit 0
	ensure
		begin
			stats = <<-EOS
Average distance: #{total_distance / n_processed}
Total latency: #{total_latency}
Total processed: #{n_processed}
Average latency: #{total_latency / n_processed}
Elapsed time: #{(Time.now - start_time).round(3)}
#{stats.inspect}
EOS

$stderr.puts stats
File.open("replayed_stat.txt", "w") {|f| f.write stats }

# output visuzlier
outfile_visualize = File.expand_path('./visualize/visualize.html', File.dirname(__FILE__))
$stderr.puts "Writing #{outfile_visualize}"
File.open(outfile_visualize, "w") do |f|
	f.write timeline.htmlify(shift_normalize: true)
end
		rescue => e
			$stderr.puts "Error while finalizing: #{e}"
		end
	end

end

if __FILE__ == $0
	require 'optparse'
	opt = OptionParser.new

	options = { }
	opt.on('-t TIMEOUT', '--timeout TIMEOUT') {|v| options[:timeout] = v.to_i }
	opt.on('-v', '--verbose') {|v| options[:verbose] = true }

	opt.parse!(ARGV)
	middleware = Middleware.new
	middleware.run(**options)
end


