package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime/pprof"
	"syscall"
)

func init() {
	fmt.Println("Listening SIGUSR1/SIGUSR2 for profiling start/stop")
	signal_chan := make(chan os.Signal, 1)
	signal.Notify(signal_chan, syscall.SIGUSR1, syscall.SIGUSR2)
	go func() {
		for {
			s := <-signal_chan
			switch s {
			case syscall.SIGUSR1:
				fmt.Println("Start Profiler")
				f, err := os.Create("/tmp/signalcpu.prof")
				if err != nil {
					log.Fatal(err)
				}
				pprof.StartCPUProfile(f)
			case syscall.SIGUSR2:
				fmt.Println("Stop Profiler")
				pprof.StopCPUProfile()
			}
		}
	}()
}

