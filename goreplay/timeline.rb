require 'json'
require 'erb'

class Timeline
	def initialize()
		@data = []
		@id = 1
	end

	# e.g. x = { "id" => (@curid += 1), "group" => unique_id, "content" => "#{unique_id} #{head}", "start" => time, "end" => time + latency }
	def <<(x)
		@data << x
		self
	end
	def add(group, content, start, stop)
		@data << { "id" => @id, "group" => group.to_s, "content" => content.to_s, "start" => start, "end" => stop }
		@id += 1
		self
	end

	def normalize()
		# shift so that the first time line is 0
		start = @data.map {|h| h["start"]}.min
		@data.map! {|h| h["start"] -= start; h["end"] -= start; h }
	end

	def htmlify(shift_normalize: false, group_numbering: false)
		normalize() if shift_normalize
		group2id = Hash[@data.map {|h| h["group"]}.select {|g| g}.uniq.each_with_index.map {|g, i| [g, i]}]
		groups = @data.map {|h| h["group"]}.select {|g| g}.uniq.map {|g| { "id" => g, "content" => "#{g}" }}
		json_groups = JSON.dump(group2id.map {|g, i| { "id" => i, "content" => (group_numbering ? i.to_s : g.to_s) }})
		json_data = JSON.dump(@data.map {|h| w = h.clone; w["group"] = group2id[w["group"]]; w})
		erb_file = File.expand_path('./visualize/visualize.erb', File.dirname(__FILE__))
		erb = ERB.new(File.open(erb_file).read)
		res = erb.result(binding)
		return res
	end
end

if $0 == __FILE__
	tl = Timeline.new
	tl << { "id" => 1, "group" => "hoge", "content" => "a", "start" => 5, "end" => 7 }
	tl << { "id" => 2, "group" => "hoge", "content" => "b", "start" => 4, "end" => 6 }
	tl << { "id" => 3, "group" => "fuga", "content" => "c", "start" => 6, "end" => 9 }
	puts tl.htmlify(group_numbering: true)
end
