package measure

import (
	"testing"
	"time"
	"fmt"
)

func TestMeasure(t *testing.T) {
	for _ = range []int{1, 2, 3} {
		Start()
		time.Sleep(time.Millisecond * 200)
		Stop()
	}
	fmt.Println(String())
}
