package measure

import (
	"time"
	"runtime"
	"fmt"
)

type Caller struct {
	funcName string
	fileName string
	line int
}

type Watch struct {
	caller Caller
	startTime time.Time
}

type Stat struct {
	count int64
	max int64
	min int64
	sum int64
}

var watchStack []Watch
var caller2elapsed = make(map[Caller]Stat)

func initStat(t int64) Stat {
	return Stat{1, t, t, t}
}
func (s Stat) add(t int64) Stat {
	s.count += 1
	if s.max < t {
		s.max = t
	}
	if s.min > t {
		s.min = t
	}
	s.sum += t
	return s
}
func (s Stat) String() string {
	return fmt.Sprintf("cnt:%d\tmax:%f\tmin:%f\tsum:%f\tavg:%f", s.count, float64(s.max) * 1e-9, float64(s.min) * 1e-9, float64(s.sum) * 1e-9, float64(s.sum) * 1e-9 / float64(s.count))
}

func Start() {
	pc, filename, line, ok := runtime.Caller(1)
	if !ok { panic("Could not detect caller.") }
	funcname := runtime.FuncForPC(pc).Name()
	watchStack = append(watchStack, Watch{Caller{funcname, filename, line}, time.Now()})
}
func Stop() {
	cur := watchStack[len(watchStack)-1]
	watchStack = watchStack[:len(watchStack)-1]
	elapsed_nanosec := time.Now().Sub(cur.startTime).Nanoseconds()
	stat, ok := caller2elapsed[cur.caller]
	if !ok {
		caller2elapsed[cur.caller] = initStat(elapsed_nanosec)
	} else {
		caller2elapsed[cur.caller] = stat.add(elapsed_nanosec)
	}
}
func String() string {
	var res string
	for k, v := range caller2elapsed {
		res += fmt.Sprintf("%s in %s line %d\n", k.funcName, k.fileName, k.line)
		res += v.String()
	}
	return res
}

